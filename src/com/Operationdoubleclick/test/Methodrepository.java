package com.Operationdoubleclick.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Methodrepository {

	static WebDriver driver;

	public static void appLaunch() {
        //Declaration of the driver and open the url
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://api.jquery.com/dblclick/");

	}

	public static void doubleclick() {
		driver.switchTo().frame(0);
		// Create the object 'action'
		Actions action = new Actions(driver);
//Find the targeted element
		WebElement elementdoubleclick = driver.findElement(By.cssSelector("html>body>div"));
		// Here I used JaentvascriptExecutor interface to scroll down to the targeted
		// element
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView();", elementdoubleclick);
		// used doubleClick(element) method to do double click action
		action.doubleClick(elementdoubleclick).build().perform();
	}

}
